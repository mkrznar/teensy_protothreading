#include "px4Serial.h"
#include "Arduino.h"

px4Serial::px4Serial()
	{
	// code executes at instancing of class
	// serial port data receive
	 _newdata = 0; // input variable for n-th (current) serial port data receive
	 _prevdata = 0; // iput variable for n-1 (previous) serial port data receive
	 _headercounter = 0; // this counts data receive that is part of heder (4byte int of [13,13,13,13]);
	 _datacounter = -1; // this counts data receive that is part of data (totaly 40 ints for qty of 10 numbers)
	}

int px4Serial::serialRawRead() {
	if (Serial3.available() > 0) {
		_newdata = Serial3.read();
		//Serial.print(_newdata);
		//Serial.println(" ");
		if (_prevdata == 13 && _newdata == 13) {
			_headercounter++;
			_datacounter = -1;
			//lengthcounter = -1;
		}
		else
		{
			_datacounter++;
			_headercounter = 0;
			if (_datacounter >= 0 && _datacounter <40) {
				_incomingBytes[_datacounter] = _newdata;
				//Serial.print(datacounter);
				//Serial.print(" ");
				//Serial.print(incomingBytes[3]);
				//Serial.print(" incomingBytes");
			}
		}


		if (_datacounter == 39) {
			serial_parse_raw_bytes();
			//Serial.print(newdata);
			//Serial.print(" ");
			//serial_print();
			//serial_print();		
		}

		//Serial.print(newdata);
		//Serial.print(" ");
		//Serial.print(headercounter);
		//Serial.print(" ");
		//Serial.print(datacounter);
		//Serial.println(" ");
		_prevdata = _newdata;
	}
} /*-----------------------------------------------*/


void px4Serial::begin() {

	Serial3.begin(115200);     // opens serial port 3, sets data rate to 115200 bps
}


/*-funkcija parsira 4byte sa seriala u float varijable u global - */
int px4Serial::serial_parse_raw_bytes() {
	
	_datareceived = false;
	//serial_print();
	//Serial.println();
	_datain[0] = _incomingBytes[0];
	_datain[1] = _incomingBytes[1];
	_datain[2] = _incomingBytes[2];
	_datain[3] = _incomingBytes[3];
	_arrayFloat[0] = *((float*)(_datain));

	_datain[0] = _incomingBytes[4];
	_datain[1] = _incomingBytes[5];
	_datain[2] = _incomingBytes[6];
	_datain[3] = _incomingBytes[7];
	_arrayFloat[1] = *((float*)(_datain));

	_datain[0] = _incomingBytes[8];
	_datain[1] = _incomingBytes[9];
	_datain[2] = _incomingBytes[10];
	_datain[3] = _incomingBytes[11];
	_arrayFloat[2] = *((float*)(_datain));

	_datain[0] = _incomingBytes[12];
	_datain[1] = _incomingBytes[13];
	_datain[2] = _incomingBytes[14];
	_datain[3] = _incomingBytes[15];
	_arrayFloat[3] = *((float*)(_datain));

	_datain[0] = _incomingBytes[16];
	_datain[1] = _incomingBytes[17];
	_datain[2] = _incomingBytes[18];
	_datain[3] = _incomingBytes[19];
	_arrayFloat[4] = *((float*)(_datain));

	_datain[0] = _incomingBytes[20];
	_datain[1] = _incomingBytes[21];
	_datain[2] = _incomingBytes[22];
	_datain[3] = _incomingBytes[23];
	_arrayFloat[5] = *((float*)(_datain));

	_datain[0] = _incomingBytes[24];
	_datain[1] = _incomingBytes[25];
	_datain[2] = _incomingBytes[26];
	_datain[3] = _incomingBytes[27];
	_arrayFloat[6] = *((float*)(_datain));

	_datain[0] = _incomingBytes[28];
	_datain[1] = _incomingBytes[29];
	_datain[2] = _incomingBytes[30];
	_datain[3] = _incomingBytes[31];
	_arrayFloat[7] = *((float*)(_datain));

	_datain[0] = _incomingBytes[32];
	_datain[1] = _incomingBytes[33];
	_datain[2] = _incomingBytes[34];
	_datain[3] = _incomingBytes[35];
	_arrayFloat[8] = *((float*)(_datain));

	_datain[0] = _incomingBytes[36];
	_datain[1] = _incomingBytes[37];
	_datain[2] = _incomingBytes[38];
	_datain[3] = _incomingBytes[39];
	_arrayFloat[9] = *((float*)(_datain));
	_datareceived = true;

	
}
/*----------------------------------------------------------------*/
/*- funkcija za pretvaranje  4bytea  float -*/
int px4Serial::readFloat(byte _datain[4])
{
	union
	{
		byte b[4];
		float f;
	} data;

	for (int i = 0; i < 4; i++)
	{
		data.b[i] = _datain[i];
	}
	return data.f;
}
/*------------------------------------------*/

/*- funkcija plota podaake primljene sa serijal portom-*/
int px4Serial::printSerialDataRead() {
	if (_datareceived == true){
	Serial.print(" Roll: ");
	Serial.print(_arrayFloat[0]);

	Serial.print(" Pitch: ");
	Serial.print(_arrayFloat[1]);

	//Serial.print("  Data2  :");
	//Serial.print(arrayFloat[1]);
	Serial.print(" Yaw: ");
	Serial.print(_arrayFloat[2]);
	//Serial.print(" 3: ");
	//Serial.print(_arrayFloat[3]);
	//Serial.print(" 4: ");
	//Serial.print(_arrayFloat[4]);
	Serial.print("| Latitude:");
	Serial.print(_arrayFloat[3]);
	Serial.print(" Longitude:");
	Serial.print(_arrayFloat[4]);
	Serial.print(" Num of satelites: ");
	Serial.print(_arrayFloat[5]);
	//Serial.print(" | ");
	//Serial.println("  ");*/
	//Serial.print(" 5: ");
	//Serial.println(_arrayFloat[5]);
	Serial.println();


	}
}
/*------------------------------------------*/




/*
float px4Serial::calculateKF(float input)
{
	// this is function inside class with float input and return of float _x_now;
	_input = input;
	//time update
	_x_now = _x_prev;
	_Pk_now = _Pk_prev + _Q;
	//	% mesurement update
	_K = _Pk_prev / (_Pk_prev + _R);// % Kalman gain.Tells how much should we trust our observations
	_x_est = _x_now + _K * (_input - _x_now);
	_Pk_now = (1 - _K)*_Pk_now;
	_Pk_prev = _Pk_now;
	_x_prev = _x_est;
	return _x_prev;
}
*/