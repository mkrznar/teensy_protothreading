#ifndef px4Seriallibrary
#define px4Seriallibrary

#if (ARDUINO >=100)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif
class px4Serial
{
public:
	// constructor
	px4Serial();
	void begin();
	int serialRawRead();
	int serial_parse_raw_bytes();
	int readFloat(byte datain[4]);
	int printSerialDataRead();
	
	//float calculateKF(float input);


private:
	byte _datain[4]; // variable used for float casting.
	// serial port data receive
	int _newdata; // input variable for n-th (current) serial port data receive
	int _prevdata; // iput variable for n-1 (previous) serial port data receive
	int _headercounter; // this counts data receive that is part of heder (4byte int of [13,13,13,13]);
	int _datacounter; // this counts data receive that is part of data (totaly 40 ints for qty of 10 numbers)
	const int _numOfDataBytes = 40;// 
	byte _incomingBytes[40];  // for incoming serial data
	float _arrayFloat[10]; // float array for variables received thru serial port
	bool _datareceived = false;
};

#endif


