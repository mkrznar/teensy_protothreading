//*********************************************************************
//Multi-threading Microcontroler OS
// RTOS 
//**********************************************************************

//**********************************************************************
//Include statementes
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SBUS.h>	 // SBUS library
#include "SimpleKFlibrary.h"
// init vage
#include <HX711t.h>// drugi library ide za teensy


#include "eeprom.h" // za citanje faktora kalibracije vage i kalibriranje
#include "MPU9250.h"

//**********************************************************************


//**********************************************************************
//Define statements
#define OLED_RESET 4
#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2
#define LOGO16_GLCD_HEIGHT 16 
#define LOGO16_GLCD_WIDTH  16

#define HWSERIAL3 Serial3 // serijski port za �itanje podataka sa pixhawka
#define HWSERIAL2 Serial2 // serijski port za sbus
#define HWSERIAL1 Serial1 // serijski port za wireles radio komunikaciju



//**********************************************************************


//**********************************************************************
//Class istancing
Adafruit_SSD1306 display(OLED_RESET);
SBUS SBUS_PX4(HWSERIAL2);// a SBUS object called SBUSToPX4, which is on Teensy hardware serial port 1 (pin 1)
HX711t scale1(6, 5);    // HX711.DOUT - pin #A1  HX711.PD_SCK - pin #A0  ---- VAGA 1
HX711t scale2(4, 3);    // HX711.DOUT - pin #A8  HX711.PD_SCK - pin #A7  ---- VAGA 2
HX711t scale3(10, 9);   // HX711.DOUT - pin #A1  HX711.PD_SCK - pin #A0  ---- VAGA 3
HX711t scale4(12, 11);  // HX711.DOUT - pin #A1  HX711.PD_SCK - pin #A0  ---- VAGA 4
SimpleKf kalmanOpto;
//**********************************************************************


//**********************************************************************
//Constants
bool mainMenuPrintedOLED;

static const unsigned char PROGMEM logo16_glcd_bmp[] =
{ B00000000, B11000000,
B00000001, B11000000,
B00000001, B11000000,
B00000011, B11100000,
B11110011, B11100000,
B11111110, B11111000,
B01111110, B11111111,
B00110011, B10011111,
B00011111, B11111100,
B00001101, B01110000,
B00011011, B10100000,
B00111111, B11100000,
B00111111, B11110000,
B01111100, B11110000,
B01110000, B01110000,
B00000000, B00110000 };



const int numOfDataBytes = 40;// ocekujemo ukupno 12 byteova
byte incomingBytes[numOfDataBytes];  // for incoming serial data
float readedFloatsInput[10];
float input1 = 0;
float input2 = 0;
float input3 = 0;
float input4 = 0;
float input5 = 0;
float input6 = 0;
float input7 = 0;
float input8 = 0;
float input9 = 0;
float input10 = 0;
byte data1[4];
byte data2[4];
byte data3[4];
byte data4[4];
byte data5[4];
byte data6[4];
byte data7[4];
byte data8[4];
byte data9[4];
byte data10[4];
byte datain[4];
int newdata;
int prev_data;
float arrayFloat[10];
int headercounter = 0;
int datacounter = -1;
int lengthcounter = -1;
bool floatSerialParsed;
uint16_t radioChannels[8]; // size for 8 input channels,radio channels values GLOBAL STATUS
uint16_t channels[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }; // write SBUS channels structure
// channel, fail safe, and lost frames data
uint8_t failSafe;
uint16_t lostFrames = 0;

// "log view studio" initialisation
boolean headersend = false; // kada je hedersend true onda se salju podaci u logview
boolean menuprinted = false; // ovo sluzi za main meni, da li krecemo prvi put ili zaustavljamo mjerenje
unsigned long intervalDurationInt = 10000; // interval at which to do something (milliseconds)				/ custom unos podataka
int numberofrepeats = 2; // broj poanvljanja pokusa
bool customPwmVector = false;




//********** thrust mesurement scale calibration constants *********
int scaleSelect;
float calibrationMass; // default mass for calibration
float scale1constant = 402.02279;
float scale2constant = 402.02279;
float scale3constant = 402.02279;
float scale4constant = 402.02279;
float scale1out, scale2out, scale3out, scale4out;
// timer and intervals 
unsigned long previousMillis = 0; // last time update
int counter = 0; // brojac za iteraciju kroz PWM
int ii = 0; // brojac za globalni broj ponavljanja
bool useDefaultPWMVector = true;

// define PWM vector varijanta 1
int pwmVectorDefault[] = { 1000, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800,
1900, 2000, 1900, 1800, 1700, 1600, 1500, 1400, 1300,
1200, 1100, 1000,1000 };
int pwmVectorSizeDefault = 23;

// Declaration of custom arrays for user input vector
int *pwmVectorCustom = 0;
int pwmVectorSizeCustom = 0;

//**********************************************************************


//**********************************************************************
//Thread definitions
unsigned long previousMicros = 0;        // will store last time LED was updated
unsigned long currentMicros = 0;

// threads interval timings
unsigned long thread1_interval = 10;           // start thread every 10 us (xxx HZ)
unsigned long thread2_interval = 50*1000;				// start thread every 50 ms (100 HZ)
unsigned long thread3_interval = 100*1000;				// start thread every 100 ms (50 HZ)
unsigned long thread4_interval = 500*1000;			// start thread every 500 ms (xx HZ)

										// threads duration track variables
unsigned long thread1_time = 0;				//  
unsigned long thread2_time = 0;				// unsigned 
unsigned long thread3_time = 0;				// 
unsigned long thread4_time = 0;

unsigned long loop_duration = 0;
unsigned long thread1_duration = 0;
unsigned long thread2_duration = 0;
unsigned long thread3_duration = 0;
unsigned long thread4_duration = 0;
unsigned long start_t1 = 0;
unsigned long start_t2 = 0;
unsigned long start_t3 = 0;
unsigned long start_t4 = 0;
float value1=1500;
float value2=1500;
char datain1[4];
char datain2[4];
int inputInt1;
int inputInt2;

//bool headersend = true;
// serial communication
const byte numChars = 32;
char receivedChars[numChars]; // an array to store the received data

boolean newData = false;

int counterC = 0;
char readString[9];



char input;





static boolean recvInProgress = false;
static byte ndx = 0;
char startMarker = '<';
char endMarker = '>';
char rc;
char messageFromPC[32] = { 0 };
int aa;
int bb;
int cc;
float readdiode;

//**********************************************************************


//**********************************************************************
//preprocesssor directives
#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif
//**********************************************************************
MPU9250 IMU(Wire, 0x68);
int status;

void setup() {

	//**********************************************************************
	//Serial ports initialisaton
	Serial.begin(9600);
	while (!Serial); // dok serial stream nije poceo ne radi nista
	HWSERIAL3.begin(57600);
	//HWSERIAL3.begin(115200);
	HWSERIAL1.begin(57600); // serijski port za wireles radio komunikaciju

	// display initialisation
	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)

	SBUS_PX4.begin();				  // begin the SBUS communication		   
	analogWriteResolution(16); // 16 bits ersolution 
	analogWriteFrequency(3, 400); // output pin and frequency in Hz

	scale1.set_scale(scale1constant);    // this value is obtained by calibrating the scale with known weights; see the README for details
	scale2.set_scale(scale2constant);
	scale3.set_scale(scale3constant);
	scale4.set_scale(scale4constant);
	

	pinMode(5, OUTPUT);
	kalmanOpto.defineRQ(1, 100);

	//display.startscrollright(0x00, 0x0F);
	//delay(2000);
	//display.stopscroll();
	//delay(1000);
	//display.startscrollleft(0x00, 0x0F);
	//delay(2000);
	//display.stopscroll();
	//****************************************************************************

	// Since the buffer is intialized with an Adafruit splashscreen internally, this will display the splashscreen.
	display.display();												// Show image buffer on the display hardware.
	display.clearDisplay();// Clear the buffer.
	initOLED();
	display.display();
	delay(1800);
	status = IMU.begin();
	if (status < 0) {
		Serial.println("IMU initialization unsuccessful");
		Serial.println("Check IMU wiring or try cycling power");
		Serial.print("Status: ");
		Serial.println(status);
		while (1) {}
	}
	
}

/*void firstThread() {
	display.clearDisplay();
	display.setTextSize(1);
	display.setCursor(0, 0);

	display.println("  TESTING TESTING");
	display.display();
	display.setTextSize(1);
	//delay(100);*/

void loop() {
		//RUN THREADS****//
	currentMicros = micros(); // get main clock, run-time

	fastloop();

	loop_duration = micros() - currentMicros;
	
	threads();

//delay(1);
	//************************************

}


//**********************************************************************
//Definitions of threads
void threads() {

	if (currentMicros >= thread1_time)
		thread1();

	if (currentMicros >= thread2_time)
		thread2();

	if (currentMicros >= thread3_time)
		thread3();

	if (currentMicros >= thread4_time)
		thread4();


}


void fastloop() {
	writeSBUS();

	//analogWrite(3, 1000 / 2500.0f*65535.0f);
	//mesurementsAndPlots();
	//readdiode = analogRead(23);
	// 
	//readdiode = kalmanOpto.calculateKF(aa);
//	digitalWrite(6, HIGH);    // Turning ON LED
	//delayMicroseconds(500);  //wait
                   //taking differnce:[ (noise+signal)-(noise)] just signal
	//aa = analogRead(23);
								  //Serial.print(a);         //noise+signal
								  //Serial.print("\t");
								  //Serial.print(b);         //noise
								  //Serial.print("\t");
//	Serial.println(c);         // denoised signal
	//Serial.print(map(aa, 50, 1000, 0, 500));


	//Serial.print('\n');
//	delay(100);

}


// start thread every 10 us (xxx HZ)	
void thread1() {
	start_t1 = micros();
	thread1_time = micros() + thread1_interval; // timing of thread
	serialRawRead();

//	HWSERIAL1.write('test');

	if (HWSERIAL3.available() > 0 && newData == false) {
		rc = HWSERIAL1.read();

		if (recvInProgress == true) {
			if (rc != endMarker) {
				receivedChars[ndx] = rc;
				ndx++;
				if (ndx >= numChars) {
					ndx = numChars - 1;
				}
			}
			else {
				receivedChars[ndx] = '\0'; // terminate the string
				recvInProgress = false;
				ndx = 0;
				newData = true;
			}
		}

		else if (rc == startMarker) {
			recvInProgress = true;
		}
	}


	if (newData == true) {
		//Serial.print("This just in ... ");
		//Serial.println(receivedChars);
		newData = false;



		char * strtokIndx; // this is used by strtok() as an index

		strtokIndx = strtok(receivedChars, ",");      // get the first part - the string
		strcpy(messageFromPC, strtokIndx); // copy it to messageFromPC

		strtokIndx = strtok(NULL, ","); // this continues where the previous call left off
		int integerFromPC = atoi(strtokIndx);     // convert this part to an integer

		strtokIndx = strtok(NULL, ",");
		int floatFromPC = atoi(messageFromPC);     // convert this part to a float

		//Serial.print("data1 ");
		///Serial.print(CAU_CAA);
		//Serial.print("  ***    data2 ");
		//Serial.println(integerFromPC);
		//Serial.print("Float ");
		//Serial.println(floatFromPC);
		aa = atoi(messageFromPC);
		bb = integerFromPC;
	}



// parsanje primnjegon podatka na dva dijela

// split the data into its parts

readSBUS(); 



	
	
		//digitalWrite(5, HIGH);
		//delayMicroseconds(500);
		
		//Serial.println("\n");
		//Serial.print("   ");
		//Serial.println(aa);

	//	analogWrite(5, aa / 4);
	//Serial.print(currentMicros);
	//Serial.println(" thread1 * ");
	//display.display();
	thread1_duration = micros() - start_t1;
	
}

// start thread every 50 ms (xxx HZ)
//pwm write
void thread2() {
	start_t2 = micros();
	thread2_time = micros() + thread2_interval;  // timing of thread


	//Serial.print(currentMicros);
	//Serial.println(" thread2 ** ");
	//display.clearDisplay();
	//display.setTextSize(1);
	//display.setCursor(0, 0);
	//display.print(" time:");
	//display.println(currentMicros);
//	display.display();
	
	

	thread2_duration = micros() - start_t2;
}

// start thread every 100 ms (xxx HZ)
void thread3() {
	start_t3 = micros();
	thread3_time = micros() + thread3_interval; // timing of thread
	//Serial.print(currentMicros);
	//Serial.println(" thread3 *** ");
	sendPWM(4, aa); // pin and pwm from 1000 to 2000
	sendPWM(6, bb);
	//Serial.print("PWM SET ");
	//Serial.print(aa);
	//Serial.print("  ** ");
	//Serial.println(bb);
					  //Serial.println(read);

		//int pwm1 = first.substring(4, 8) //get the first four characters
		//servo2 = 
		

		/*char c = Serial.read();
		switch (c)
		{
		case 83: // "S" -> Start // ascii kod, broj 83 je S  http://www.theasciicode.com.ar/ascii-printable-characters/capital-letter-e-uppercase-ascii-code-69.html
			logViewInit();
			headersend = true;
			break;

		case 115: // "s" -> Start 
			logViewInit();
			break;*/
		

	thread3_duration = micros() - start_t3;
	
}

// start thread every 500 ms (xxx HZ)
// serial print and oled screen prints
void thread4() {
	start_t4 = micros();
	thread4_time = start_t4 + thread4_interval; // timing of thread
		//Serial.println(" thread4printSerialReadData();
	//printReadSBUS_OLED();
	//display.print(arrayFloat[0]);
	//display.println(arrayFloat[1]);
	//if (floatSerialParsed == true) {
	//printSerialReadDataOLED();
	//}
	//printSerialReadDataOLED();
	//printDataSerial();
	printDataOLED();
	//display.print(thread4_time);
	//printSerialReadData();
	if (headersend)
	{
	//	sendToSerial();

	}

	IMU.readSensor();
	// display the data
	Serial.print("getAccelX_mss");
	Serial.print(IMU.getAccelX_mss(), 6);
	Serial.print("\t");
	Serial.print("getAccelY_mss");
	Serial.print(IMU.getAccelY_mss(), 6);
	Serial.print("\t");
	Serial.print("getAccelZ_mss");
	Serial.print(IMU.getAccelZ_mss(), 6);
	Serial.println("\t");
	Serial.print("getGyroX_rads");
	Serial.print(IMU.getGyroX_rads(), 6);
	Serial.print("\t");
	Serial.print("getGyroY_rads");
	Serial.print(IMU.getGyroY_rads(), 6);
	Serial.print("\t");
	Serial.print("getGyroZ_rads");
	Serial.print(IMU.getGyroZ_rads(), 6);
	Serial.println("\t");
	Serial.print("getMagX_uT");
	Serial.print(IMU.getMagX_uT(), 6);
	Serial.print("\t");
	Serial.print("getMagY_uT");
	Serial.print(IMU.getMagY_uT(), 6);
	Serial.print("\t");
	Serial.print("getMagZ_uT");
	Serial.print(IMU.getMagZ_uT(), 6);
	Serial.println("\t");
	Serial.print("getTemperature_C");
	Serial.println(IMU.getTemperature_C(), 6);

	thread4_duration = micros() - start_t4;
	//Serial.println(thread4_duration);
}
//***********************************************************************


//*****FUNCTIONS




void logViewInit()
{
	String tempst;
	Serial.println("Start detected");
	// Send Header information
	tempst = "$N$;Data Logging\r\n";      // data logging je text hedera
	Serial.print(tempst);
	tempst = "$C$; roll 1; pitch 2; yaw 3 ; ch 4\r\n";   // ovdje upisujemo vage
	Serial.print(tempst);

	}

void logViewTerminate()
{
	Serial.println("End detected");
	headersend = false;
	menuprinted = false;  // ponovno ispisujem meni
}

// funkcija za  ispis u konzolu u LogView Formatu;  sendToSerial();
void takeMesurementsFromScales() {
	scale1out = scale1.get_units(3);

	scale2out = scale2.get_units(3);

	scale3out = scale3.get_units(3);

	scale4out = scale4.get_units(3);

}

void sendToSerial()
{
	// TO BE DONE, provjeri ispisivanje u konzolu....
	//Serial.print(pwmVectorSizeDefault);
	//Serial.print(pwmVectorSizeCustom);
	
	

		Serial.print("$");
		Serial.print(arrayFloat[0] * 180/3.14);
		Serial.print(";");
		Serial.print(arrayFloat[1] * 180 / 3.14);
		Serial.print(";");
		Serial.print(arrayFloat[2] * 180 / 3.14);
		Serial.print(";");
		Serial.print(arrayFloat[3] * 180 / 3.14);
		Serial.print("\r\n"); // plotting
	}


void initOLED() {
	//display.clearDisplay();
	//display init
	display.drawLine(110, 20, 100, 25, WHITE); // draw trianlge
	display.drawLine(100, 25, 120, 25, WHITE); // draw trianlge
	display.drawLine(120, 25, 110, 20, WHITE); // draw trianlge
	display.drawCircle(111, 22, 6, WHITE); // cricle
										   //display.width()
										   //display.height()
										   //delay(2000);
	display.setCursor(0, 0);
	display.setTextColor(BLACK, WHITE);
	display.println("     TriviumRTOS     ");
	display.setTextColor(WHITE);
	display.println(" ");
	display.println("initial release");
	display.println("version 0.1 ");
	//display.display();

}


void printDataOLED() {
	
	display.clearDisplay();
	int sum = thread1_duration + thread2_duration + thread3_duration + thread4_duration;

	float t_1 = 100*thread1_duration / sum;
	float t_2 = 100*thread2_duration / sum;
	float t_3 = 100*thread3_duration / sum;
	float t_4 = 100*thread4_duration / sum;// / sum;
	display.setCursor(0, 0);
	display.println("multitask (uS ,%)");
	display.print("%:");
	display.print(t_1,1);
	display.print(" ");
	display.print(t_2,1);
	display.print(" ");
	display.print(t_3,1);
	display.print(" ");
	display.println(t_4,1);
	display.print(" T1:");
	display.print(thread1_duration);
	display.print(" ");
	display.print(" T2:");
	display.println(thread2_duration);
	display.print(" T3:");
	display.print(thread3_duration);
	display.print(" ");
	display.print(" T4:");
	display.print(thread4_duration);
	display.display();
}



void printDataSerial() {
	
	Serial.println("multitask (uS ,%)");
	Serial.print("Loop:");
	Serial.println(loop_duration);
	Serial.print(" Task1:");
	Serial.print(thread1_duration);
	Serial.print(" ");
	Serial.print(" Task2:");
	Serial.println(thread2_duration);
	Serial.print(" Task3:");
	Serial.print(thread3_duration);
	Serial.print(" ");
	Serial.print(" Task4:");
	Serial.print(thread4_duration);

}






// funkcija za generiranje PWM, cmd je od 1000 do 2000 
void sendPWM(int pin,  float Cmd) {
	analogWrite(pin, Cmd / 2500.0f*65535.0f);
}




/*** funkcija �ita pakete byteva sa serial porta ***/
int serialRawRead() {
	if (HWSERIAL3.available() > 0) {

		newdata = HWSERIAL3.read();
		//Serial.print(newdata);
		//Serial.println(" ");
		if (prev_data == 13 && newdata == 13) {
			headercounter++;
			datacounter = -1;
			//lengthcounter = -1;
		}
		else
		{
			datacounter++;
			headercounter = 0;
			if (datacounter >= 0 && datacounter <40) {
				incomingBytes[datacounter] = newdata;
				//Serial.print(datacounter);
				//Serial.print(" ");
				//Serial.print(incomingBytes[3]);
				//Serial.print(" incomingBytes");

			}
		}


		if (datacounter == 39) {
			serial_parse_raw_bytes();
			//Serial.print(newdata);
			//Serial.print(" ");
			//serial_print();
			//serial_print();		
		}

		//Serial.print(newdata);
		//Serial.print(" ");
		//Serial.print(headercounter);
		//Serial.print(" ");
		//Serial.print(datacounter);
		//Serial.println(" ");
		prev_data = newdata;
	}
}
/**********************************************/

// funkcija parsira 4byte sa seriala u float varijable u global
int serial_parse_raw_bytes() {
	floatSerialParsed = false;
	//serial_print();
	//Serial.println();
	datain[0] = incomingBytes[0];
	datain[1] = incomingBytes[1];
	datain[2] = incomingBytes[2];
	datain[3] = incomingBytes[3];
	arrayFloat[0] = readFloat(datain);//*((float*)(data1));

	datain[0] = incomingBytes[4];
	datain[1] = incomingBytes[5];
	datain[2] = incomingBytes[6];
	datain[3] = incomingBytes[7];
	arrayFloat[1] = readFloat(datain);//*((float*)(data2));

	datain[0] = incomingBytes[8];
	datain[1] = incomingBytes[9];
	datain[2] = incomingBytes[10];
	datain[3] = incomingBytes[11];
	arrayFloat[2] = readFloat(datain);//*((float*)(data3));

	datain[0] = incomingBytes[12];
	datain[1] = incomingBytes[13];
	datain[2] = incomingBytes[14];
	datain[3] = incomingBytes[15];
	arrayFloat[3] = readFloat(datain);//*((float*)(data4));

	datain[0] = incomingBytes[16];
	datain[1] = incomingBytes[17];
	datain[2] = incomingBytes[18];
	datain[3] = incomingBytes[19];
	arrayFloat[4] = readFloat(datain);//*((float*)(data5));


	datain[0] = incomingBytes[20];
	datain[1] = incomingBytes[21];
	datain[2] = incomingBytes[22];
	datain[3] = incomingBytes[23];
	arrayFloat[5] = readFloat(datain);//*((float*)(data6));


	datain[0] = incomingBytes[24];
	datain[1] = incomingBytes[25];
	datain[2] = incomingBytes[26];
	datain[3] = incomingBytes[27];
	arrayFloat[6] = readFloat(datain);//*((float*)(data7));


	datain[0] = incomingBytes[28];
	datain[1] = incomingBytes[29];
	datain[2] = incomingBytes[30];
	datain[3] = incomingBytes[31];
	arrayFloat[7] = readFloat(datain);//*((float*)(data8));

	datain[0] = incomingBytes[32];
	datain[1] = incomingBytes[33];
	datain[2] = incomingBytes[34];
	datain[3] = incomingBytes[35];
	arrayFloat[8] = readFloat(datain);//*((float*)(data9));

	datain[0] = incomingBytes[36];
	datain[1] = incomingBytes[37];
	datain[2] = incomingBytes[38];
	datain[3] = incomingBytes[39];
	arrayFloat[9] =readFloat(datain);//*((float*)(data10));

	floatSerialParsed = true;

}
/**********************************************/


/*** funkcija za pretvaranje  4bytea  float ***/
float readFloat(byte datain[4])
{
	union
	{
		byte b[4];
		float f;
	} data;

	for (int i = 0; i < 4; i++)
	{
		data.b[i] = datain[i];
	}
	return data.f;
}
/**********************************************/


/*** funkcija za ispis primljenig serial podataka ***/
int printSerialReadData() {

	Serial.print(" Roll: ");
	Serial.print(arrayFloat[0]);

	Serial.print(" Pitch: ");
	Serial.print(arrayFloat[1]);

	//Serial.print("  Data2  :");
	//Serial.print(arrayFloat[1]);

	Serial.print(" Yaw: ");
	Serial.println(arrayFloat[2]);


	/*Serial.print("D1:");
	Serial.print(input1);
	Serial.print(" D2:");
	Serial.print(input2);
	Serial.print(" D3:");
	Serial.print(input3);
	Serial.print(" D4:");
	Serial.print(input4);
	Serial.print(" D5:");
	Serial.print(input5);
	Serial.print(" D6:");
	Serial.print(input6);
	Serial.print(" D7:");
	Serial.print(input7);
	Serial.print(" D8:");
	Serial.print(input8);
	Serial.print(" D9:");
	Serial.println(input9);*/
	return 0;
}
/**********************************************/


/*** funkcija za ispis primljenig serial na OLED ekran ***/
int printSerialOLED() {

	printSerialReadDataOLED();
	
}


/*** funkcija za ispis primljenig serial na OLED ekran ***/
int printSerialReadDataOLED() {
	display.setTextSize(1);
	display.setCursor(0, 0);
	display.print(" Roll: ");
	display.println(arrayFloat[0]);

	display.print(" Pitch: ");
	display.println(arrayFloat[1]);

	display.print(" Yaw: ");
	display.println(arrayFloat[2]);
}


void readSBUS() {
	SBUS_PX4.read(&channels[0], &failSafe, &lostFrames);
	//	Serial.println(channels[0]);
	//	Serial.println(channels[1]);
	//	Serial.println(failSafe);
}
/**********************************************/


void writeSBUS() {
	channels[0] = 0;
	channels[1] = aa;
	channels[2] = 0;
	channels[3] = bb;
	channels[4] = 0;
	channels[5] = 0;
	channels[6] = 0;
	channels[7] = 0;

	SBUS_PX4.write(&channels[0]);
	//	Serial.println(channels[0]);
	//	Serial.println(channels[1]);
	//	Serial.println(failSafe);
}
/**************

void printReadSBUS() {
	Serial.print(channels[0]);
	Serial.print("  ");
	Serial.print(channels[1]);
	Serial.print("  ");
	Serial.print(channels[2]);
	Serial.print("  ");
	Serial.print(radioChannels[3]);
	Serial.print("  ");
	Serial.print(radioChannels[4]);
	Serial.print("  ");
	Serial.print(radioChannels[5]);
	Serial.print("  ");
	Serial.print(radioChannels[6]);
	Serial.print("  ");
	Serial.print(radioChannels[7]);
	Serial.println("  ");

}
/**********************************************/


void printReadSBUS_OLED() {
	display.setCursor(0, 0);
	display.print(channels[0]);
	display.print(" ");
	display.print(channels[1]);
	display.println(" ");
	display.print(channels[2]);
	display.print(" ");
	display.print(channels[3]);
	display.print(" ");
	display.println(channels[4]);
	
	
	
	
/*	Serial.print(" ");
	Serial.print(radioChannels[5]);
	Serial.print(" ");
	Serial.print(radioChannels[6]);
	Serial.print(" ");
	Serial.print(radioChannels[7]);
	Serial.println(" ");
	*/
}



/// DISPLAY FUNCTIONS

/*


void testdrawbitmap(const uint8_t *bitmap, uint8_t w, uint8_t h) {
uint8_t icons[NUMFLAKES][3];

// initialize
for (uint8_t f = 0; f< NUMFLAKES; f++) {
icons[f][XPOS] = random(display.width());
icons[f][YPOS] = 0;
icons[f][DELTAY] = random(5) + 1;

Serial.print("x: ");
Serial.print(icons[f][XPOS], DEC);
Serial.print(" y: ");
Serial.print(icons[f][YPOS], DEC);
Serial.print(" dy: ");
Serial.println(icons[f][DELTAY], DEC);
}

while (1) {
// draw each icon
for (uint8_t f = 0; f< NUMFLAKES; f++) {
display.drawBitmap(icons[f][XPOS], icons[f][YPOS], bitmap, w, h, WHITE);
}
display.display();
delay(200);

// then erase it + move it
for (uint8_t f = 0; f< NUMFLAKES; f++) {
display.drawBitmap(icons[f][XPOS], icons[f][YPOS], bitmap, w, h, BLACK);
// move it
icons[f][YPOS] += icons[f][DELTAY];
// if its gone, reinit
if (icons[f][YPOS] > display.height()) {
icons[f][XPOS] = random(display.width());
icons[f][YPOS] = 0;
icons[f][DELTAY] = random(5) + 1;
}
}
}
}


void testdrawchar(void) {
display.setTextSize(1);
display.setTextColor(WHITE);
display.setCursor(0, 0);

for (uint8_t i = 0; i < 168; i++) {
if (i == '\n') continue;
display.write(i);
if ((i > 0) && (i % 21 == 0))
display.println();
}
display.display();
delay(1);
}

void testdrawcircle(void) {
for (int16_t i = 0; i<display.height(); i += 2) {
display.drawCircle(display.width() / 2, display.height() / 2, i, WHITE);
display.display();
delay(1);
}
}

void testfillrect(void) {
uint8_t color = 1;
for (int16_t i = 0; i<display.height() / 2; i += 3) {
// alternate colors
display.fillRect(i, i, display.width() - i * 2, display.height() - i * 2, color % 2);
display.display();
delay(1);
color++;
}
}

void testdrawtriangle(void) {
for (int16_t i = 0; i<min(display.width(), display.height()) / 2; i += 5) {
display.drawTriangle(display.width() / 2, display.height() / 2 - i,
display.width() / 2 - i, display.height() / 2 + i,
display.width() / 2 + i, display.height() / 2 + i, WHITE);
display.display();
delay(1);
}
}

void testfilltriangle(void) {
uint8_t color = WHITE;
for (int16_t i = min(display.width(), display.height()) / 2; i>0; i -= 5) {
display.fillTriangle(display.width() / 2, display.height() / 2 - i,
display.width() / 2 - i, display.height() / 2 + i,
display.width() / 2 + i, display.height() / 2 + i, WHITE);
if (color == WHITE) color = BLACK;
else color = WHITE;
display.display();
delay(1);
}
}

void testdrawroundrect(void) {
for (int16_t i = 0; i<display.height() / 2 - 2; i += 2) {
display.drawRoundRect(i, i, display.width() - 2 * i, display.height() - 2 * i, display.height() / 4, WHITE);
display.display();
delay(1);
}
}

void testfillroundrect(void) {
uint8_t color = WHITE;
for (int16_t i = 0; i<display.height() / 2 - 2; i += 2) {
display.fillRoundRect(i, i, display.width() - 2 * i, display.height() - 2 * i, display.height() / 4, color);
if (color == WHITE) color = BLACK;
else color = WHITE;
display.display();
delay(1);
}
}

void testdrawrect(void) {
for (int16_t i = 0; i<display.height() / 2; i += 2) {
display.drawRect(i, i, display.width() - 2 * i, display.height() - 2 * i, WHITE);
display.display();
delay(1);
}
}

void testdrawline() {
for (int16_t i = 0; i<display.width(); i += 4) {
display.drawLine(0, 0, i, display.height() - 1, WHITE);
display.display();
delay(1);
}
for (int16_t i = 0; i<display.height(); i += 4) {
display.drawLine(0, 0, display.width() - 1, i, WHITE);
display.display();
delay(1);
}
delay(250);

display.clearDisplay();
for (int16_t i = 0; i<display.width(); i += 4) {
display.drawLine(0, display.height() - 1, i, 0, WHITE);
display.display();
delay(1);
}
for (int16_t i = display.height() - 1; i >= 0; i -= 4) {
display.drawLine(0, display.height() - 1, display.width() - 1, i, WHITE);
display.display();
delay(1);
}
delay(250);

display.clearDisplay();
for (int16_t i = display.width() - 1; i >= 0; i -= 4) {
display.drawLine(display.width() - 1, display.height() - 1, i, 0, WHITE);
display.display();
delay(1);
}
for (int16_t i = display.height() - 1; i >= 0; i -= 4) {
display.drawLine(display.width() - 1, display.height() - 1, 0, i, WHITE);
display.display();
delay(1);
}
delay(250);

display.clearDisplay();
for (int16_t i = 0; i<display.height(); i += 4) {
display.drawLine(display.width() - 1, 0, 0, i, WHITE);
display.display();
delay(1);
}
for (int16_t i = 0; i<display.width(); i += 4) {
display.drawLine(display.width() - 1, 0, i, display.height() - 1, WHITE);
display.display();
delay(1);
}
delay(250);
}

void testscrolltext(void) {
display.setTextSize(2);
display.setTextColor(WHITE);
display.setCursor(10, 0);
display.clearDisplay();
display.println("scroll");
display.display();
delay(1);

display.startscrollright(0x00, 0x0F);
delay(2000);
display.stopscroll();
delay(1000);
display.startscrollleft(0x00, 0x0F);
delay(2000);
display.stopscroll();
delay(1000);
display.startscrolldiagright(0x00, 0x07);
delay(2000);
display.startscrolldiagleft(0x00, 0x07);
delay(2000);
display.stopscroll();
}

























// draw a single pixel
display.drawPixel(10, 10, WHITE);	// Show the display buffer on the hardware.
// NOTE: You _must_ call display after making any drawing commands
// to make them visible on the display hardware!
display.display();
delay(1000);
display.clearDisplay();

// draw many lines
testdrawline();
display.display();
delay(2000);
display.clearDisplay();

// draw rectangles
testdrawrect();
display.display();
delay(2000);
display.clearDisplay();

// draw multiple rectangles
testfillrect();
display.display();
delay(2000);
display.clearDisplay();

// draw mulitple circles
testdrawcircle();
display.display();
delay(2000);
display.clearDisplay();

// draw a white circle, 10 pixel radius
display.fillCircle(display.width() / 2, display.height() / 2, 10, WHITE);
display.display();
delay(2000);
display.clearDisplay();

testdrawroundrect();
delay(2000);
display.clearDisplay();

testfillroundrect();
delay(2000);
display.clearDisplay();

testdrawtriangle();
delay(2000);
display.clearDisplay();

testfilltriangle();
delay(2000);
display.clearDisplay();

// draw the first ~12 characters in the font
testdrawchar();
display.display();
delay(2000);
display.clearDisplay();

// draw scrolling text
testscrolltext();
delay(2000);
display.clearDisplay();

// text display tests
display.setTextSize(1);
display.setTextColor(WHITE);
display.setCursor(0, 0);
display.println("Hello, world!");
display.setTextColor(BLACK, WHITE); // 'inverted' text
display.println(3.141592);
display.setTextSize(2);
display.setTextColor(WHITE);
display.print("0x"); display.println(0xDEADBEEF, HEX);
display.display();
delay(2000);
display.clearDisplay();

// miniature bitmap display
display.drawBitmap(30, 16, logo16_glcd_bmp, 16, 16, 1);
display.display();
delay(1);

// invert the display
display.invertDisplay(true);
delay(1000);
display.invertDisplay(false);
delay(1000);
display.clearDisplay();
// draw a bitmap icon and 'animate' movement
testdrawbitmap(logo16_glcd_bmp, LOGO16_GLCD_HEIGHT, LOGO16_GLCD_WIDTH);
*/
